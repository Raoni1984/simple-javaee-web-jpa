<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html" %>
<%@ page pageEncoding="utf-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
</head>
<body>

<div style="margin: 30px">
    <p>
    <form method="post">
        <label>First Name:
            <input type="text" name="first_name"/>
        </label>
        <br/>
        <label>Last Name:
            <input type="text" name="last_name"/>
        </label>
        <br/>
        <br/>
        <input type="submit" value="Add New Customer"/>
    </form>
    </p>
</div>

<div>
    <p>
        <c:forEach items="${customers}" var="customer">
            <c:out value="${customer.customer_id}"/> &nbsp;
            <c:out value="${customer.first_name}"/> &nbsp;
            <c:out value="${customer.last_name}"/> &nbsp;
            <c:out value="${customer.last_update}"/>
            <br/>
        </c:forEach>
    </p>
</div>
</body>
</html>
