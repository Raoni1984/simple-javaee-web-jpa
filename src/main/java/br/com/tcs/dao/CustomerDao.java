package br.com.tcs.dao;

import br.com.tcs.model.Customer;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

@SuppressWarnings("unchecked")
public class CustomerDao {

    public List<Customer> getAllCustomers() {
        EntityManager em = PersistenceUtil.getEntityManager();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        List<Customer> actors = em.createQuery("FROM Customer").getResultList();
        transaction.commit();
        return actors;
    }

    public Customer getWithId(int id) {
        EntityManager em = PersistenceUtil.getEntityManager();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        Customer actor = em.find(Customer.class, id);
        transaction.commit();
        return actor;
    }

    public void addNewCustomer(String first_name, String last_name) {
        Customer actor = new Customer();
        actor.setFirst_name(first_name);
        actor.setLast_name(last_name);
        EntityManager entityManager = PersistenceUtil.getEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.merge(actor);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
        }
    }
}
